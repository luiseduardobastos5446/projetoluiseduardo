@extends('template')
@section('conteudo')
    <br>
    <br>
    <br>
    <h2> Olá, esse é meu site onde pretendo mostrar tudo o que faço e todos os meus projetos na área da programação! </h2>

    <br>

    <ul class="list-unstyled">
        <div class="shadow-sm p-3 mb-5 bg-white rounded">
            <li class="media">
                <img class="align-self-center mr-3" src="img/perfil.png" alt="Imagem" style="width: 250px; border-radius: 30px;">
                <div class="media-body">
                    <h5 class="mt-0 mb-1"><b>Luis Eduardo Bastos Rocha</b></h5>
                    <br>
                    <p style="text-align: justify">Aluno do <b>Instituto Federal de Ciência e Tecnologia de Minas Gerais - Campus Ouro Preto</b>.</p>
                    <p style="text-align: justify">Faz parte do projeto de extensão <b>ProgramaAção</b> e atua como <b>monitor de alguns cursos de nível inicial e intermediário de programação de computadores</b>.</p>
                    <br>
                    <p style="text-align: justify"><b>Meus Projetos e Cursos: </b><a href="{{ route('projetos')}}" > <span class="btn btn-info">Projetos</span> </a></p>
                    <p style="text-align: justify"><b>Cursos que oferece monitoria: </b><a href="{{ route('projetosalgoritmos')}}" > <span class="btn btn-info">Algoritmos</span> </a>, <a href="{{ route('projetoslingc')}}" > <span class="btn btn-info">Linguagem C</span> </a>, <a href="{{ route('projetosligcpp')}}" > <span class="btn btn-info">Linguagem C++</span></a> e <a href="{{ route('projetosappinv')}}" > <span class="btn btn-info">App Inventor</span></b></a>.</p>
                </div>
            </li>
        </div>
    </ul>

    <h3>Certificados</h3>
    <br>

    <div class="certificados">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/cert-alg.png" alt="Imagem de capa do card">
            <div class="card-body">
                <h5 class="card-title">Algoritmos</h5>
                <p class="card-text">Certificado obtido por fazer o curso de Algoritmos - Introdução à Programação com sucesso.</p>
                <a href="{{ route('certalgoritmos')}}" class="btn btn-primary">Visitar</a>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/cert-lingc.png" alt="Imagem de capa do card">
            <div class="card-body">
                <h5 class="card-title">Linguagem C</h5>
                <p class="card-text">Certificado obtido por fazer o curso de Linguagem C - Introdução à Programação com sucesso.</p>
                <a href="{{ route('certlingc')}}" class="btn btn-primary">Visitar</a>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/cert-lingcpp.png" alt="Imagem de capa do card">
            <div class="card-body">
                <h5 class="card-title">Linguagem C++</h5>
                <p class="card-text">Certificado obtido por fazer o curso de Linguagem C++ - Introdução à Programação Intermediária com sucesso.</p>
                <a href="{{ route('certlingcpp')}}" class="btn btn-primary">Visitar</a>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/cert-appinv.png" alt="Imagem de capa do card">
            <div class="card-body">
                <h5 class="card-title">App Inventor</h5>
                <p class="card-text">Certificado obtido por fazer o curso de App Inventor - Introdução à Criação de Aplicativos Móveis com sucesso.</p>
                <a href="{{ route('certappinv')}}" class="btn btn-primary">Visitar</a>
            </div>
        </div>
    </div>

    <br>

    <div class="container">
        <img class="align-self-center mr-3" src="img/banner-projle.png" alt="Imagem" style="width: 1000px;">
    </div>

    <br>
    <br>

    @stop
@section('rodape')
    
@stop