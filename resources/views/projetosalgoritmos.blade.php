@extends('template')
@section('conteudo')
    <br>
    <br>
    <br>
    <h2> Curso Algoritmos </h2>

    <br>

    <ul class="list-unstyled">
        <div class="shadow-sm p-3 mb-5 bg-white rounded">  
        <li class="media">
            <div class="media-body">
                <a href="{{ route('projetos')}}" > <span class="btn btn-info">VOLTAR</span> </a>
                <br> 
                <br>
                <img class="align-self-center mr-3" src="{!! asset('img/algoritmos.png') !!}" alt="Imagem">
                <p>O objetivo principal deste curso é introduzir a fundamentação teórica e prática na área de <b>Algoritmos e Programação</b>.
                    Este curso é composto de atividades educativas remotas organizadas no âmbito do <b>Projeto de Extensão Programa Ação da Coordenadoria do Curso de Automação Industrial/CODAAUT do IFMG – Campus Ouro Preto</b> e é direcionado a alunos do 9º ano do Ensino Fundamental e alunos do 1º ano do Curso Técnico Integrado de Automação Industrial do IFMG campus Ouro Preto. </p>
                    <p><b>Professores:</b> Adolfo José S. Baudson, Francisco César R. de Araújo, Lucas Emiliano de Souza Moreira e Osvaldo Novais Junior (coordenador). <br><b>Monitores:</b> Luís Eduardo Bastos e Luiz Miguel Gonçalves (bolsistas do Projeto de Extensão Programa Ação)</p>
            </div>
        </li>
    </ul>

    <br>
    <br>

@stop
@section('rodape')
    
@stop