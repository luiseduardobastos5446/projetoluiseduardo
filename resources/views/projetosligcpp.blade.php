@extends('template')
@section('conteudo')
    <br>
    <br>
    <br>
    <h2> Curso Linguagem C++ </h2>

    <br>

    <ul class="list-unstyled">
        <li class="media">
        <div class="shadow-sm p-3 mb-5 bg-white rounded">
            <div class="media-body">
                <a href="{{ route('projetos')}}" > <span class="btn btn-info">VOLTAR</span> </a> 
                <br>
                <br>
                <img class="align-self-center mr-3" src="{!! asset('img/lingcpp.png') !!}" alt="Imagem">
                <p>Este treinamento é uma parceria do <b>Projeto de Extensão Programa Ação da CODAAUT - IFMG Campus Ouro Preto com o Projeto de Extensão opCod3rs do DECOM - UFOP</b>. Este treinamento visa aprimorar os conhecimentos de programação dos alunos do <b>Curso Técnico de Automação Industrial do IFMG Campus Ouro Preto</b> para participação na OBI 2022, Maratona Intercampi do IFMG e outras competições de programação.</p>
                    <p><b>Professores:</b> Adolfo José S. Baudson, Francisco César R. de Araújo, Lucas Emiliano de Souza Moreira e Osvaldo Novais Junior (coordenador). <br><b>Monitores:</b> Luís Eduardo Bastos e Luiz Miguel Gonçalves (bolsistas do Projeto de Extensão Programa Ação)</p>
            </div>
        </div>
        </li>
    </ul>

    <br>
    <br>

@stop
@section('rodape')
    
@stop