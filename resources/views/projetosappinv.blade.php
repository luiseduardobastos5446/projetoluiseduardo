@extends('template')
@section('conteudo')
    <br>   
    <br>
    <br>
    <h2> Curso APP Inventor </h2>

    <br>

    <ul class="list-unstyled">
        <li class="media">
        <div class="shadow-sm p-3 mb-5 bg-white rounded">
            <div class="media-body">
                <a href="{{ route('projetos')}}" > <span class="btn btn-info">VOLTAR</span> </a> 
                <br>
                <br>
                <img class="align-self-center mr-3" src="{!! asset('img/appinv.png') !!}" alt="Imagem">
                <p>Programa de atividades remotas sobre computação móvel com o App Inventor: princípios de <b>Ciência da Computação</b>. Como o título sugere, este programa de atividades busca ensinar os <b>princípios da ciência da computação na perspectiva da computação móvel</b>.</p>
            </div>
        </div>
        </li>
    </ul>

    <br>
    <br>

@stop
@section('rodape')
    
@stop