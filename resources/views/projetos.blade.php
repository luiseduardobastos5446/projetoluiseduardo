@extends('template')
@section('conteudo')
    <br>
    <br>
    <br>
    <h2> Projetos e Cursos</h2>

    <br>

    <div class="projetos">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/algoritmos.png" alt="Imagem de capa do card" style="width: 270px; padding-top: 28px; padding-left: 10px; padding-right: 10px">
            <div class="card-body">
                <br>
                <br>
                <h5 class="card-title">Algoritmos</h5>
                <p class="card-text"><p>O objetivo principal deste curso é introduzir a fundamentação teórica e prática na área de <b>Algoritmos e Programação</b>.
                    <p><b>Professores:</b> Adolfo José S. Baudson, Francisco César R. de Araújo, Lucas Emiliano de Souza Moreira e Osvaldo Novais Junior (coordenador). <br><b>Monitores:</b> Luís Eduardo Bastos e Luiz Miguel Gonçalves (bolsistas do Projeto de Extensão Programa Ação)</p></p></p>
                    <br>
                <a href="{{ route('projetosalgoritmos')}}" class="btn btn-primary">Visitar</a>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/lingc.png" alt="Imagem de capa do card" style="width: 270px; padding: 10px">
            <div class="card-body">
                <h5 class="card-title">Linguagem C</h5>
                <p class="card-text"><p>O objetivo principal deste curso é introduzir a fundamentação teórica e prática na área de <b>Programação com a Linguagem C</b>.</p>
                    <p><b>Professores:</b> Adolfo José S. Baudson, Francisco César R. de Araújo, Lucas Emiliano de Souza Moreira e Osvaldo Novais Junior (coordenador). <br><b>Monitores:</b> Luís Eduardo Bastos e Luiz Miguel Gonçalves (bolsistas do Projeto de Extensão Programa Ação)</p>
                <a href="{{ route('projetoslingc')}}" class="btn btn-primary">Visitar</a>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/lingcpp.png" alt="Imagem de capa do card" style="width: 270px; padding: 10px">
            <div class="card-body">
                <h5 class="card-title">Linguagem C++</h5>
                <p class="card-text"><p>O objetivo principal deste curso é introduzir a fundamentação teórica e prática na área de <b>Programação com a Linguagem C</b>.</p>
                    <p><b>Professores:</b> Adolfo José S. Baudson, Francisco César R. de Araújo, Lucas Emiliano de Souza Moreira e Osvaldo Novais Junior (coordenador). <br><b>Monitores:</b> Luís Eduardo Bastos e Luiz Miguel Gonçalves (bolsistas do Projeto de Extensão Programa Ação)</p></p>
                    <br>
                <a href="{{ route('projetosligcpp')}}" class="btn btn-primary">Visitar</a>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/appinv.png" alt="Imagem de capa do card" style="width: 270px; padding: 10px">
            <div class="card-body">
                <h5 class="card-title">App Inventor</h5>
                <p class="card-text"><p>Programa de atividades remotas sobre computação móvel com o App Inventor: princípios de <b>Ciência da Computação</b>.</p>
                    <p><b>Professores:</b> Adolfo José S. Baudson, Francisco César R. de Araújo, Lucas Emiliano de Souza Moreira e Osvaldo Novais Junior (coordenador). <br><b>Monitores:</b> Luís Eduardo Bastos e Luiz Miguel Gonçalves (bolsistas do Projeto de Extensão Programa Ação)</p></p>
                    <br>
                <a href="{{ route('projetosappinv')}}" class="btn btn-primary">Visitar</a>
            </div>
        </div>
    </div>

    <br>

    <div class="container">
        <img class="align-self-center mr-3" src="img/banner-projle.png" alt="Imagem" style="width: 1000px;">
    </div>

    <br>
    <br>

@stop
@section('rodape')
    
@stop