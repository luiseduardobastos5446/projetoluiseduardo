@extends('template')
@section('conteudo')
    <br>
    <br>
    <br>
    <h2> Obrigado pela mensagem! </h2>
    <br>
    
    <p>Enviamos uma cópia de sua mensagem para seu email</p>
    
    <br>
    <br>

    <ul class="list-unstyled">
        <li class="media">
            <div class="media-body">
                <a href="{{ route('contato')}}" > <span class="btn btn-info">VOLTAR</span> </a> 
            </div>
        </li>
    </ul>

    <br>
    <br>

@stop
@section('rodape')
    
@stop