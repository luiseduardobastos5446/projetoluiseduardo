@extends('template')
@section('conteudo')
    <br>
    <br>
    <br>
    <h2> Contato </h2>

    <div class="shadow-sm p-3 mb-5 bg-white rounded">
        <form class="contato" method="submit" action="contatosucesso">
            <div class="form-row">
                <div class="form-group col-md-6">
                <label for="text">Nome</label>
                <input type="text" class="form-control" placeholder="Nome">
                </div>
                <div class="form-group col-md-6">
                <label for="text">Sobrenome</label>
                <input type="text" class="form-control" placeholder="Sobrenome">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Tipo de Contato</label>
                <select class="form-control" id="select">
                <option>Selecione uma opção</option>
                <option>Dúvida</option>
                <option>Crítica</option>
                <option>Elogio</option>
                <option>Contato Direto</option>
                <option>Outro</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Digite sua mensagem:</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Mensagem"></textarea>
            </div>
            <div class="form-group">
                <div class="form-check">
                <input class="form-check-input" type="checkbox" id="gridCheck">
                <label class="form-check-label" for="gridCheck">
                    Receber cópia em meu email?
                </label>
                </div>
            </div>
            <button type="submit" class="btn btn-info">Enviar</button>
        </form>
    </div>

    <div class="container">
        <img class="align-self-center mr-3" src="img/banner-projle.png" alt="Imagem" style="width: 1000px;">
    </div>

    <br>
    <br>

@stop
@section('rodape')
    
@stop