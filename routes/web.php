<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/home', function () {
    return view('home');
})->name('home');

Route::get('/projetos', function () {
    return view('projetos');
})->name('projetos');

Route::get('/contato', function () {
    return view('contato');
})->name('contato');

Route::get('/projetosalgoritmos', function () {
    return view('projetosalgoritmos');
})->name('projetosalgoritmos');

Route::get('/projetoslingc', function () {
    return view('projetoslingc');
})->name('projetoslingc');

Route::get('/projetosligcpp', function () {
    return view('projetosligcpp');
})->name('projetosligcpp');

Route::get('/projetosappinv', function () {
    return view('projetosappinv');
})->name('projetosappinv');

Route::get('/certalgoritmos', function () {
    return view('certalgoritmos');
})->name('certalgoritmos');

Route::get('/certlingc', function () {
    return view('certlingc');
})->name('certlingc');

Route::get('/certlingcpp', function () {
    return view('certlingcpp');
})->name('certlingcpp');

Route::get('/certappinv', function () {
    return view('certappinv');
})->name('certappinv');

Route::get('/contatosucesso', function () {
    return view('contatosucesso');
})->name('contatosucesso');